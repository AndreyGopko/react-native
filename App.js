import React, { createRef } from 'react';
import {
    ScrollView,
    ActivityIndicator,
    View,
    StyleSheet,
    TextInput,
} from 'react-native';

import {
    Header,
    Card,
    Text,
    Button,
    Icon,
    Divider,
    CheckBox,
} from 'react-native-elements';

const generateId = () => Math.random();

export default class ToDoList extends React.Component {
    input = createRef();
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            value: '',
            list: [],
            editingElementId: null,
            editingValue: ''
        }
    }

    onAddItem = () => {
        if (this.state.value.trim()) {
            this.setState({
                list: [
                    ...this.state.list,
                    {
                        id: generateId(),
                        text: this.state.value,
                        done: false
                    }
                ],
                value: '',
            })

            this.input.current.blur();
        }
    }

    onRemoveItem = (id) => {
        const updatedList = this.state.list.filter(item => item.id !== id)

        this.setState({
            list: updatedList,
        })
    }

    onEditItem = (id) => {
        const index = this.state.list.findIndex(item => item.id === id);
        const editedItem = this.state.list[index];

        this.setState({ editingElementId: id, editingValue: editedItem.text });
    }

    onEditFinishItem = (id) => {
        if (!this.state.editingValue.trim()) return;

        const updatedList = this.state.list.map(item => (
            (item.id === id) ? { ...item, text: this.state.editingValue } : item
        ))

        this.setState({
            list: updatedList,
            editingElementId: null,
            editingValue: '',
        })
    }

    onToggleDone = (id) => {
        const updatedList = this.state.list.map(item => (
            (item.id === id) ? {...item, done: !item.done} : item
        ))

        this.setState({
            list: updatedList,
        })
    }

    renderAddForm() {
        return (
            <View style={{ marginBottom: 10 }}>
                <TextInput
                    placeholder='Input text...'
                    onChangeText={(value) => this.setState({ value })}
                    value={this.state.value}
                    style={{ height: 40, fontSize: 18, margin: 15 }}
                    ref={this.input}
                />
                <Button
                    icon={{ name: 'add', color: '#fff' }}
                    backgroundColor='#03A9F4'
                    buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                    title='Add ToDo Item'
                    onPress={this.onAddItem}
                />
            </View>
        );
    }

    renderTextItem(item) {
        if (this.state.editingElementId === item.id) {
            return (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <TextInput
                        placeholder='Input text...'
                        onChangeText={(value) => this.setState({ editingValue: value })}
                        value={this.state.editingValue}
                        style={{ height: 40, fontSize: 18, margin: 15, flex: 1 }}
                    />
                    <Icon
                        raised
                        name='save'
                        color='#00b972'
                        size={20}
                        containerStyle={{ backgroundColor: '#eee', margin: 5 }}
                        underlayColor='#ddd'
                        onPress={() => this.onEditFinishItem(item.id)}
                    />
                </View>
            )
        }

        return (
            <View>
                <CheckBox
                    title={item.text}
                    containerStyle={{ marginLeft: 0, marginRight: 0 }}
                    textStyle={{ fontSize: 18 }}
                    checked={item.done}
                    onPress={() => this.onToggleDone(item.id)}
                />
            </View>
        )
    }

    renderListItems() {
        return (
            <ScrollView>
                {this.state.list.map(item => (
                    <Card key={item.id}>
                        {this.renderTextItem(item)}
                        <Divider style={{ backgroundColor: '#ccc' }} />
                        <View style={{ flexDirection: 'row' }}>
                            <Icon
                                raised
                                name='clear'
                                color='#f00'
                                size={20}
                                containerStyle={{ backgroundColor: '#eee', margin: 5 }}
                                underlayColor='#ddd'
                                onPress={() => this.onRemoveItem(item.id)}
                            />
                            <Icon
                                raised
                                name='create'
                                color='#f90'
                                size={20}
                                containerStyle={{ backgroundColor: '#eee', margin: 5 }}
                                underlayColor='#ddd'
                                onPress={() => this.onEditItem(item.id)}
                            />
                        </View>
                    </Card>
                ))}
            </ScrollView>
        )
    }

    renderNoCards() {
        if (!this.state.list.length) {
            return (
                <View>
                    <Text>There is no more cards</Text>
                </View>
            );
        }
        return null;
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={[styles.common, styles.centered]}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }

        return (
            <View style={{ flex: 1 }}>
                <Header
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: 'TO DO LIST', style: { color: '#fff' } }}
                    rightComponent={{ icon: 'home', color: '#fff' }}
                />
                <View style={styles.common}>
                    {this.renderAddForm()}
                    {this.renderNoCards()}
                    {this.renderListItems()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    common: {
        flex: 1,
        padding: 20,
    },
    centered: {
        justifyContent: 'center',
        alignItems: 'center',
    }
})